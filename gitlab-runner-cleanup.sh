#!/bin/sh

limit=80

if test -d /var/srv/storage; then
	# have instance storage (see gitlab-runner-storage.sh)
	fs=/var/srv/storage
else
	fs=/var/lib
fi

usage=$(df --output=pcent $fs | grep -v -i use | tr -d "%")

if test "$usage" -lt "$limit"; then
    # all fine
    exit 0
fi

# go cleanup
docker images prune -f -a
docker volumes prune -f
