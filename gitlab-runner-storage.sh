#!/bin/sh

# config
storage="/var/srv/storage"
stordirs="
/var/lib/containers
/var/lib/docker
"

# detect environment
case "$(cat /sys/class/dmi/id/sys_vendor)" in
    QEMU | Red*)
        echo "# qemu detected"
        pattern="0QEMU_QEMU_HARDDISK_hd" # testing: scsi target 42
        ;;
    Amazon*)
        echo "# aws detected"
        pattern="Instance_Storage"
        ;;
    *)
        echo "# unknown system"
        pattern="UNKNOWN"
        ;;
esac

#############################################################################
# go

if mount | grep -q ${storage}; then
    echo "# container storage already mounted"
    exit 0
fi

blkdev="$(ls /dev/disk/by-id/*${pattern}* | grep -v -e '-part' | head -n 1)"
if test ! -b "${blkdev}"; then
    echo "# no storage (${pattern}) found"
    exit 0
fi

echo "# try mount ${blkdev} as container storage"
mkdir -p ${storage}
if mount -t xfs -v ${blkdev} ${storage}; then
    echo "# OK"
else
    echo "# setup and mount ${blkdev} as container storage"
    (
        set -ex
        dd if=/dev/zero of=${blkdev} bs=65536 count=1024
        mkfs.xfs -L containers ${blkdev}
        mount -t xfs -v ${blkdev} ${storage}
        restorecon ${storage}
    )
fi

echo "# setup bind mounts to storage"
for dir in $stordirs; do
    name="${dir//\//}"
    mkdir -p "${storage}/${name}" "${dir}"
    mount --verbose --bind "${storage}/${name}" "${dir}"
done
