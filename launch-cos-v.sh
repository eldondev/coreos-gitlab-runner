IGNITION_CONFIG="gitlab.ign"
IMAGE="fedora-coreos-37.20230205.3.0-qemu.x86_64.qcow2"
# for x86/aarch64:
IGNITION_DEVICE_ARG="-fw_cfg name=opt/com.coreos/config,file=${IGNITION_CONFIG}"

# for s390x/ppc64le:
#IGNITION_DEVICE_ARG="-drive file=${IGNITION_CONFIG},if=none,format=raw,readonly=on,id=ignition -device virtio-blk,serial=ignition,drive=ignition"

qemu-system-x86_64 -m $((8096 * 4)) -smp 8 -machine accel=kvm -cpu host,vmx=on -nographic  \
	-monitor unix:qemu-monitor-socket-$(date +%s),server,nowait \
	${IGNITION_DEVICE_ARG} \
	-kernel fedora-coreos-37.20230205.3.0-live-kernel-x86_64 \
        -initrd fedora-coreos-37.20230205.3.0-live-initramfs.x86_64.img \
	-nic user,model=virtio,hostfwd=tcp::2223-:22 \
	-device virtio-scsi-pci,id=scsi0 \
	-device scsi-hd,bus=scsi0.0,drive=hd \
        -drive if=none,id=hd,file=/media/scratch.qcow2 \
        -append "ignition.platform.id=qemu console=tty0 console=ttyS0,115200n8 ignition.firstboot ip=dhcp nameserver=8.8.8.8 coreos.live.rootfs_url=http://10.0.2.2:8000/fedora-coreos-37.20230205.3.0-live-rootfs.x86_64.img"
        #-append "ignition.platform.id=qemu console=tty0 console=ttyS0,115200n8 ignition.firstboot ip=dhcp nameserver=8.8.8.8 coreos.live.rootfs_url=https://builds.coreos.fedoraproject.org/prod/streams/stable/builds/37.20230205.3.0/x86_64/fedora-coreos-37.20230205.3.0-live-rootfs.x86_64.img"
	#-drive if=virtio,file=${IMAGE} \
