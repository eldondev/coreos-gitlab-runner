
## gitlab runner for Fedora CoreOS

# Quickstart

 * `dnf install butane`
 * `cp gitlab-runner.sysconfig.example gitlab-runner.sysconfig`
 * Edit `gitlab-runner.sysconfig`, fill in your tokens.
 * `make`
 * boot Fedora CoreOS with `gitlab.ign`

# Storage

When additional storage is detected the VM will be configured to use
that for docker containers and volumes.  The disk will be formated if
needed.  Supported:

 * AWS Instance Storage
 * Qemu: virtio-scsi disks attached as target 42.

See `gitlab-runner-storage.sh`.
