
base	:= $(shell git rev-parse --show-toplevel)
bu	:= $(shell git ls-files *.bu)
ign	:= $(patsubst %.bu,%.ign,$(bu))
cmd	:= butane --pretty --strict --files-dir=$(base)

default: $(ign)

clean:
	rm -f $(ign)

%.ign: %.bu
	@echo "# --- create $@ ---"
	$(cmd) < $< > $@
	@echo ""

gitlab.ign: gitlab.bu \
	gitlab-runner.service \
	gitlab-runner-storage.sh \
	gitlab-runner-register.service \
	gitlab-runner-register.sh \
	gitlab-runner-unregister.sh \
	gitlab-runner-cleanup.service \
	gitlab-runner-cleanup.sh \
	gitlab-runner.sysconfig \
	gitlab-runner.config.toml

