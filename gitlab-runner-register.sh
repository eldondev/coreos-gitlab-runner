#!/bin/sh

if test ! -f /etc/sysconfig/gitlab-runner; then
    echo "not found: /etc/sysconfig/gitlab-runner"
    exit 1
fi
source /etc/sysconfig/gitlab-runner

########################################################################

args=()
args+=("--non-interactive")
args+=("--url" "${GITLAB_SERVER-https://gitlab.com}")

# arch tags + aliases
tags="$(uname -m)"
case "$(uname -m)" in
    x86_64)
	tags="$tags,amd64"
	args+=("--run-untagged")	# default arch
	;;
    aarch64)
	tags="$tags,arm64"
	;;
esac

# distro tags
if test -f /etc/os-release; then
    source /etc/os-release
    tags="$tags,${ID}"
    tags="$tags,${ID}-${VERSION_ID}"
fi

# environment
virt="$(systemd-detect-virt --vm)"
name="$(hostname -f)"
case "$virt" in
    none)
        # bare metal
        ;;
    amazon)
        instance="$(cat /sys/class/dmi/id/board_asset_tag)"
        type="$(cat /sys/class/dmi/id/product_name)"
        tags="$tags,amazon,${type},${instance}"
        name="${instance}"
        ;;
    kvm)
        tags="$tags,kvm"
        ;;
    *)
        tags="$tags,$virt"
        ;;
esac

# using docker
tags="$tags,docker"
args+=("--executor" "docker")
args+=("--docker-image" "docker:stable")
args+=("--docker-privileged")
args+=("--docker-volumes" "/certs/client")
# finalize name + tags
args+=("--name" "$name")
args+=("--tag-list" "$tags")

########################################################################

echo "# register gitlab-runners"
count=0
total=$(echo ${GITLAB_TOKENS} | wc -w)
for token in ${GITLAB_TOKENS}; do
    count=$(( $count + 1 ))
    echo "# $count/$total ..."
    (set -x; /bin/docker run --rm \
	--name "gitlab-runner-register.${count}" \
	--volume /etc/gitlab-runner:/etc/gitlab-runner \
	gitlab/gitlab-runner:latest \
        register "${args[@]}" --registration-token "$token")
done

jobs=$(($(nproc)-2))
#jobs=2
echo "# configure concurrent (${jobs})"
sed -e "/concurrent/s/[0-9]\+/${jobs}/" -i /etc/gitlab-runner/config.toml
