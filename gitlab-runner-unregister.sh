#!/bin/sh

########################################################################

echo "# unregister gitlab-runners"
/bin/docker run --rm \
    --name "gitlab-runner-unregister" \
    -v /etc/gitlab-runner:/etc/gitlab-runner \
    gitlab/gitlab-runner:latest \
    unregister --all-runners
